# Telerik Academy Stage Forum -> Test Case Ideas about forum topics

Prepared by: Bozhidara-Michelle Vicheva

URL: https://stage-forum.telerikacademy.com/

## Summary
The purpose of this document is to outline the Priority and Test Design Techniques applied when testing
the forum's topic functionalities.

## Features to be tested
- Topic Creation
- Commenting
- Notifications

## Test Design Techniques to be applied
- Boundary value analysis
- Pairwise technique
- Use case testing

## Test Case Ideas
|     Test Case Title                                                                                                      | Description                                                                                                                                                                                                                                                  |     Priority |     Technique    |
|--------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------|------------------|
| Topic title   should not be empty.                                                                                       | A Title is required for every   topic. An error notification should pop up if attempted otherwise.                                                                                                                                                           | Prio   1     | BVA   / Pairwise |
| Topic   title should contain at least 5 characters.                                                                      | A Title should be between 5 and   255 characters in length. An error notification should pop up if attempted   otherwise.                                                                                                                                    | Prio 2       | BVA /   Pairwise |
| Topic   title should contain no more than 255 characters.                                                                | A Title should be between 5 and   255 characters in length. An error notification should pop up if attempted   otherwise.                                                                                                                                    | Prio 2       | BVA /   Pairwise |
| A   "welcome" message notification should pop up for first time users.                                                   | A "welcome message"   notification with a valid link to the forum's community guidelines should pop   up when a new topic title is being entered by a first time user.                                                                                       | Prio 3       | Use Case testing |
| A   "related topic" message notification should pop up for existing   group of keywords in the title.                    | A "Your topic is similar   to…" message notification should pop up with links to existing threads,   where the group of keywords, typed in the new topic's title already occur .                                                                             | Prio 3       | Use Case testing |
| Topic   body should not be empty.                                                                                        | A Title is required for every   topic. An error notification should pop up if attempted otherwise. An error   notification should pop up if attempted otherwise.                                                                                             | Prio 1       | BVA /   Pairwise |
| Topic   body should be ate least 10 characters long.                                                                     | Topic body content should be ate   least 10 characters long.                                                                                                                                                                                                 | Prio 2       | BVA /   Pairwise |
| An   "abandon draft" prompt notification should pop up if a populated   topic is being cancelled.                        | An "abandon draft"   prompt notification with choices to keep or abandon should pop up if the   "cancel" button is clicked on an already correctly populated topic.                                                                                          | Prio 2       | Use Case testing |
| Bold and Italic   styles should work together.                                                                           | Both examples of typographical   emphasis should be applied simultaneously to the selected text.                                                                                                                                                             | Prio 3       | Pairwise         |
| Comment   body should not be empty.                                                                                      | Some content is required for   every post. An error notification should pop up if attempted otherwise. An   error notification should pop up if attempted otherwise.                                                                                         | Prio 1       | BVA /   Pairwise |
| A   "revive this topic" message notification should pop up if the last   activity on a topic is older than 6 months ago. | A "revive this topic"   message should pop up and notify the user that commenting on a  topic, dormant for longer than 6 months   would bump it to the top of its list and will send notifications to anyone   previously involved in said old conversation. | Prio 3       | Use Case testing |