package com.telerikacademy;

import java.lang.reflect.Method;
import java.time.LocalDate;

public class Main {

    public static void main(String[] args) {
        BoardItem item = new BoardItem("Registration doesn't work", LocalDate.now().plusDays(2));
        item.advanceStatus();
        BoardItem anotherItem = new BoardItem("Encrypt user data", LocalDate.now().plusDays(10));

        Board.items.add(item);
        Board.items.add(anotherItem);

        for (BoardItem boardItem : Board.items) {
            boardItem.advanceStatus();
        }

        for (BoardItem boardItem : Board.items) {
            System.out.println(boardItem.viewInfo());
        }


        BoardItem thirdItem = new BoardItem("Rewrite everything", LocalDate.now().plusDays(2));

        // compilation error if you uncomment the next line:
        // thirdItem.title = "Rewrite everything immediately!!!";
        // we made title private so it cannot be accessed directly anymore

        //thirdItem.setTitle("Rewrite everything immediately!!!"); // properly changing the title
        System.out.println(thirdItem.getTitle()); // properly accessing the title
        //thirdItem.setTitle(null); // Exception thrown: Please provide a title with length between 5 and 30 chars

        EventLog log = new EventLog("An important thing happened");
        System.out.println(log.getDescription());
        System.out.println(log.viewInfo());

        EventLog log2 = new EventLog(null);

    }

}
