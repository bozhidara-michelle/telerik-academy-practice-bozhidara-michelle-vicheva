package com.telerikacademy;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class EventLog {

    private final String description;
    private final String timeStamp;

    public EventLog(String description) {
        if (description == null) {
            throw new IllegalArgumentException("Please provide a non-empty name");
        }
        this.description = description;
        this.timeStamp = DateTimeFormatter.ofPattern("dd-MMMM-yy hh:mm:ss").format(LocalDateTime.now());
    }


    public String getDescription() {
        return description;
    }

    public String getTimeStamp() {
        return timeStamp;
    }

    public String viewInfo() {
        return String.format("[%s] %s", this.timeStamp,  this.description);

    }

}

