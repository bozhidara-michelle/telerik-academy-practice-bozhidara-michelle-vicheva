package com.telerikacademy;

import java.time.LocalDate;

public class BoardItem {

    private final Status initialStatus = Status.OPEN;
    private final Status finalStatus = Status.VERIFIED;

    private String title;
    private LocalDate dueDate;
    private Status status;

    public BoardItem(String title, LocalDate dueDate) {
        this.title = title;
        this.dueDate = dueDate;
        this.status = initialStatus;
    }

    private void setTitle(String title) {
        if (title == null) {
            throw new IllegalArgumentException("Please provide a non-empty name");
        }
        if (title.length() < 5 || title.length() > 30) {
            throw new IllegalArgumentException("Please provide a name with length between 5 and 30 chars");
        }
        this.title = title;
    }

    public String getTitle() {
        return title;
    }

    private void setDueDate(LocalDate dueDate) {
        if (dueDate.isBefore(LocalDate.now())) {
            throw new IllegalArgumentException("DueDate can't be in the past");
        }
        this.dueDate = dueDate;
    }

    public LocalDate getDueDate() {
        return dueDate;
    }

    public Status getStatus() {
        return status;
    }

    public void revertStatus() {
        if (this.status != initialStatus) {
            status = Status.values()[status.ordinal() - 1];
        }
    }

    public void advanceStatus() {
        if (this.status != finalStatus) {
            Status oldStatus = getStatus();
            status = Status.values()[status.ordinal() + 1];
        }
    }

    public String viewInfo() {
        return String.format("'%s', [%s | %s]", this.title, this.status, this.dueDate);
    }

}
