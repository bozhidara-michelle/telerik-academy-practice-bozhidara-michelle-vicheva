package com.telerikacademy.oop;

public class Person {
    public String name;

    public void sayName() {
        System.out.printf("Hi, my name is %s.", name);
    }
}
