package com.telerikacademy;

public class ForumPost {

    String author;
    String text;
    int upVotes;

    public ForumPost(String author, String text, int upVotes) {
        this.author = author;
        this.text = text;
        this.upVotes = upVotes;
    }

}
