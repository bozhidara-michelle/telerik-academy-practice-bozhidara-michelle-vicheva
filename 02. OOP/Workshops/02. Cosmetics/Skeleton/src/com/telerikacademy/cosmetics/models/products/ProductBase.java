package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public abstract class ProductBase implements Product {
    //Finish the class
    //implement proper interface (see contracts package)
    //validate

    final static int MIN_NAME_SYMBOLS = 3;
    final static int MAX_NAME_SYMBOLS = 10;
    final static int MIN_BRAND_NAME_SYMBOLS = 2;
    final static int MAX_BRAND_NAME_SYMBOLS = 10;

    private String name;
    private String brand;
    private double price;
    private GenderType gender;


    ProductBase(String name, String brand, double price, GenderType gender) {
        setName(name);
        setBrand(brand);
        setPrice(price);
        //this.gender = gender;
    }

    @Override
    public String getName() {
        return name;
    }

    private void setName(String name) {
        if (name==null) {
            throw new IllegalArgumentException("Null exception thrown");
        } else if (name.length() >= MIN_NAME_SYMBOLS && name.length() <= MAX_NAME_SYMBOLS) {
            this.name = name;
        } else {
            throw new IllegalArgumentException("Minimum name’s length is " + MIN_NAME_SYMBOLS +
                    " symbols and maximum is " + MAX_NAME_SYMBOLS + " symbols.");
        }
    }

    @Override
    public String getBrand() {
        return brand;
    }

    private void setBrand(String brand) {
        if(brand==null){
            throw new IllegalArgumentException("Null exception thrown");
        }
        else if (brand.length() >= MIN_BRAND_NAME_SYMBOLS && brand.length() <= MAX_BRAND_NAME_SYMBOLS) {
            this.brand = brand;
        } else {
            throw new IllegalArgumentException("Minimum name’s length is " + MIN_BRAND_NAME_SYMBOLS +
                    " symbols and maximum is " + MAX_BRAND_NAME_SYMBOLS + " symbols.");
        }
    }

    @Override
    public double getPrice() {
        return price;
    }

    private void setPrice(double price) {
        if (price >= 0) {
            this.price = price;
        } else {
            throw new IllegalArgumentException("Price cannot be negative.");
        }
    }

    @Override
    public GenderType getGender() {
        return gender;
    }

    @Override
    public String print() {
        return String.format(" #%s %s \n #Price: $%.2f \n #Gender: %s",
                getName(), getBrand(), getPrice(), getGender().toString());
    }

}
