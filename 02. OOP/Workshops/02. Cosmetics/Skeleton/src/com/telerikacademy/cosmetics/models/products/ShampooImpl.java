package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

public class ShampooImpl extends ProductBase implements Shampoo {

    private int milliliters;
    UsageType usageType;
    
    public ShampooImpl(String name, String brand, double price,
                       GenderType gender, int milliliters, UsageType everyDay) {
        super(name, brand,price, gender);
        setMilliliters(milliliters);
    }

    @Override
    public int getMilliliters() {
        return milliliters;
    }
    public void setMilliliters(int milliliters) {
        if(milliliters>=0){
            this.milliliters = milliliters;
        }

    }


    @Override
    public String print(){
        return String.format(super.print() + "\n #Milliliters: %d \n #Usage: %s \n ===",
                getMilliliters(),getUsage().toString());
    }


    @Override
    public UsageType getUsage() {
        return usageType;
    }
}
