package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

import java.util.ArrayList;
import java.util.List;

public class Category {
    
    private static final int NAME_MIN_LENGTH = 2;
    private static final int NAME_MAX_LENGTH = 15;
    private static final String NAME_INVALID_MESSAGE = "Name should be between 2 and 15 symbols.";
    private static final String DELETE_PRODUCT_ERROR_MESSAGE = "Product not found in category.";
    
    private String name;
    private List<Product> products;
    
    public Category(String name) {
        setName(name);
        products = new ArrayList<>();
    }
    
    private void setName(String name) {
        if (name.length() < NAME_MIN_LENGTH || name.length() > NAME_MAX_LENGTH) {
            throw new IllegalArgumentException(NAME_INVALID_MESSAGE);
        }
        this.name = name;
    }
    
    public List<Product> getProducts() {
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        ValidationHelper.checkProductNotNull(product);
        products.add(product);
    }
    
    public void removeProduct(Product product) {
        if (!products.contains(product)) {
            throw new IllegalArgumentException(DELETE_PRODUCT_ERROR_MESSAGE);
        }
        products.remove(product);
    }
    
    public String print() {
        StringBuilder result = new StringBuilder();
        result.append(String.format("#Category: %s\n", name));
        
        if (products.isEmpty()) {
            result.append(" #No product in this category\n");
        }
        
        for (Product product : products) {
            result.append(product.print());
        }
        
        return result.toString();
    }
    
}
