package com.telerikacademy.oop.cosmetics.commands;

import com.telerikacademy.oop.cosmetics.commands.contracts.Command;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsFactory;
import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsRepository;
import com.telerikacademy.oop.cosmetics.models.Category;

import java.util.List;

public class CreateCategory implements Command {
    
    private CosmeticsRepository cosmeticsRepository;
    private CosmeticsFactory cosmeticsFactory;
    
    public CreateCategory(CosmeticsFactory cosmeticsFactory, CosmeticsRepository cosmeticsRepository) {
        this.cosmeticsFactory = cosmeticsFactory;
        this.cosmeticsRepository = cosmeticsRepository;
    }
    
    @Override
    public String execute(List<String> parameters) {
        String categoryName = parameters.get(0);
        return createCategory(categoryName);
    }
    
    private String createCategory(String categoryName) {
        if (cosmeticsRepository.getCategories().containsKey(categoryName)) {
            return String.format(CommandConstants.CATEGORY_EXISTS, categoryName);
        }
        
        Category category = cosmeticsFactory.createCategory(categoryName);
        cosmeticsRepository.getCategories().put(categoryName, category);
        
        return String.format(CommandConstants.CATEGORY_CREATED, categoryName);
    }
    
}
