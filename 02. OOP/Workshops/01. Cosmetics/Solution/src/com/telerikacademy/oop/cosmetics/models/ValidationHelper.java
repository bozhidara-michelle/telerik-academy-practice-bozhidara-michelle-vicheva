package com.telerikacademy.oop.cosmetics.models;

import com.telerikacademy.oop.cosmetics.models.products.Product;

public class ValidationHelper {
    
    private static final String INVALID_PRODUCT_MESSAGE = "Product can't be null.";
    
    public static void checkProductNotNull(Product product) {
        if (product == null) {
            throw new IllegalArgumentException(INVALID_PRODUCT_MESSAGE);
        }
    }
    
}
