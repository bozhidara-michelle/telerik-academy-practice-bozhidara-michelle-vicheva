package com.telerikacademy.oop.cosmetics.core.factories;

import com.telerikacademy.oop.cosmetics.core.contracts.CosmeticsFactory;
import com.telerikacademy.oop.cosmetics.models.Category;
import com.telerikacademy.oop.cosmetics.models.cart.ShoppingCart;
import com.telerikacademy.oop.cosmetics.models.products.Product;

public class CosmeticsFactoryImpl implements CosmeticsFactory {
    
    public Category createCategory(String name) {
        return new Category(name);
    }
    
    public Product createProduct(String name, String brand, double price, String gender) {
        throw new UnsupportedOperationException("Not implemented yet.");
    }
    
    public ShoppingCart createShoppingCart() {
        return new ShoppingCart();
    }
    
}
