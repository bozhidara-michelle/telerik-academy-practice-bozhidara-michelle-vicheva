package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train {

    private final String TRAIN_PASSENGER_CAPACITY_RANGE = "A train cannot have less than 30 passengers or more than 150 passengers.";
    private final String TRAIN_CART_CAPACITY_RANGE = "A train cannot have less than 1 cart or more than 15 carts.";

    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setCarts(carts);
    }

    @Override
    public void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity<30 || passengerCapacity>150){
            throw new IllegalArgumentException(TRAIN_PASSENGER_CAPACITY_RANGE);
        }
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public int getCarts() {
        return carts;
    }
    private void setCarts(int carts) {
        if(carts<1 || carts >15){
            throw new IllegalArgumentException(TRAIN_CART_CAPACITY_RANGE);
        }
        this.carts= carts;
    }

    @Override
    public String print() {
        return String.format("%s%d"+System.lineSeparator(),super.print()+
                "Carts amount: ",getCarts());
    }
}
