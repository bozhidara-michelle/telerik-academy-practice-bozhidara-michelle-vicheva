package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus {

    private final String BUS_CAPACITY_RANGE = "A bus cannot have less than 10 passengers or more than 50 passengers.";

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
    }

    @Override
    public void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity < 10 || passengerCapacity > 50) {
            throw new IllegalArgumentException(BUS_CAPACITY_RANGE);
        }
        this.passengerCapacity = passengerCapacity;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    @Override
    public String print() {
        return String.format("%s",super.print());
    }

/*    @Override
    public String toString() {
        return this.print();
    }*/
}
