package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public abstract class VehicleBase implements Vehicle {
    
    //finish the class

    private final String VEHICLE_CAPACITY_RANGE = "A vehicle with less than 1 passengers or more than 800 passengers cannot exist!";
    private final String PRICE_PER_KILOMETER_RANGE = "A vehicle with a price per kilometer lower than \\$0.10 or higher than \\$2.50 cannot exist!";
    private final String TYPE_CANT_BE_NULL = "The vehicle's type cannot be null";

    int passengerCapacity;
    private double pricePerKilometer;
    public VehicleType type;
    
    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {
        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        if (type == null)
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        this.type = type;
    }

    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    public void setPassengerCapacity(int passengerCapacity) {
        if (passengerCapacity<1 || passengerCapacity>800){
            throw new IllegalArgumentException(VEHICLE_CAPACITY_RANGE);
        }
        this.passengerCapacity = passengerCapacity;
    }

    public double getPricePerKilometer() {
        return pricePerKilometer;
    }

    public void setPricePerKilometer(double pricePerKilometer) {
        if(pricePerKilometer<0.10 || pricePerKilometer >2.50){
            throw new IllegalArgumentException(PRICE_PER_KILOMETER_RANGE);
        }
        this.pricePerKilometer = pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return type;
    }

    public String printClassName() {
        return this.getClass().getSimpleName().replace("Impl", "");
    }

    @Override
    public String print() {
        return String.format("%s ----"+ System.lineSeparator() +
                "Passenger capacity: %d"+ System.lineSeparator() +
                "Price per kilometer: %.2f"+ System.lineSeparator() +
                "Vehicle type: %s" + System.lineSeparator(), printClassName(), getPassengerCapacity(), getPricePerKilometer(), getType());
    }

    @Override
    public String toString() {
        return print();
    }
}
