package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private final static String CATEGORY_FIELD = "Category";
    public final static String CATEGORY_CANNOT_BE_NULL = CATEGORY_FIELD + " cannot be null!";
    private String category;

    //look in DealershipFactoryImpl - use it to create proper constructor

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, vehicleType.MOTORCYCLE);
        setCategory(category);
    }

    @Override
    public String getCategory() {
        return category;
    }

    private void setCategory(String category) {
        Validator.ValidateNull(category, String.format(ModelsConstants.FIELD_CANNOT_BE_NULL, CATEGORY_FIELD));
        Validator.ValidateIntRange(category.length(), ModelsConstants.MIN_CATEGORY_LENGTH, ModelsConstants.MAX_CATEGORY_LENGTH,
                String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX,
                        CATEGORY_FIELD, ModelsConstants.MIN_CATEGORY_LENGTH, ModelsConstants.MAX_CATEGORY_LENGTH));
        this.category = category;
    }

    @Override
    public VehicleType getType() {
        return vehicleType.MOTORCYCLE;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %s", CATEGORY_FIELD, category);
    }
}




