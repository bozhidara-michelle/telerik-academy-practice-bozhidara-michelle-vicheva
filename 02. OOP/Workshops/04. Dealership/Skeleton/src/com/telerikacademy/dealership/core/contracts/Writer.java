package com.telerikacademy.dealership.core.contracts;

public interface Writer {
    
    void write(String message);
    
    void writeLine(String message);
    
}
