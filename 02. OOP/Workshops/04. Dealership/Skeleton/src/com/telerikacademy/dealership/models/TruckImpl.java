package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Validator;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {

    private final static String CAPACITY_FIELD = "Weight Capacity";
    private final static String CAPACITY_FIELD_LOWER_CASE = "Weight capacity";
    private int weightCapacity;

    //look in DealershipFactoryImpl - use it to create proper constructor

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, vehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }
    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }

    private void setWeightCapacity(int weightCapacity) {
        Validator.ValidateIntRange(weightCapacity, ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY,
                String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX,
                        CAPACITY_FIELD_LOWER_CASE, ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY));
        this.weightCapacity = weightCapacity;
    }

    @Override
    public VehicleType getType() {
        return vehicleType.TRUCK;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  %s: %dt", CAPACITY_FIELD, weightCapacity);
    }
}
